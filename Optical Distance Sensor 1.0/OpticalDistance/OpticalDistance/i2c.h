#ifndef I2C_h
#define I2C_h
	void read_bytes(uint8_t * data, uint8_t * receive, uint8_t n);
	void write_bytes(uint8_t * data, uint8_t n);
	void write_byte(uint8_t slave_addr, uint16_t reg_addr, uint8_t data);
#endif