/*
 * OpticalDistance.c
 *
 * Created: 11/17/2014
 *  Author: Patrick
 */
#define TEST FALSE
 
#define SLAVE_ADDR 0x52
#define TWI_READ 0x1
#define TWI_WRITE 0x0

#define RANGE_INIT 0
#define RANGE_READY_REQUEST 1
#define RANGE_READY_READ 2
#define RANGE_READY_GET 3
#define RANGE_DATA_REQUEST 4
#define RANGE_DATA_READ 5
#define RANGE_DATA_GET 6
#define RANGE_RESET_INIT 7
#define RANGE_RESET_REQUEST 8
#define RANGE_RESET_READ 9
#define RANGE_RESET_GET 10

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "TWI_Master.h"

static uint32_t globalCount = 0;

uint16_t test1;
uint8_t spiRequest;
uint8_t TWI_transBuff[10], TWI_recBuff[10];
uint8_t range_status;
uint8_t distance;

void processSPI(void);
void spi_setup(void);
uint8_t spi_slave_receive(void);
uint8_t get_reading(void);
/*
int main(void) {
	spi_setup();
	TWI_Master_Initialise();
	sei();     // Enable interrupts
	
	DDRB |= (1<<DDRB1);
	DDRB &= ~(1<<DDRB0);
	DDRD |= (1<<DDRD6) | (1<<DDRD7);
	PORTB &= ~(1<<PORTB1);
	PORTD |= (1<<PORTD6) | (1<<PORTD7);
	range_status = RANGE_INIT;
	TWI_recBuff[1] = 1;
	distance = 0;
	while(1) {
		globalCount++;
		if(globalCount == 250000)
		{
			PORTB |= (1<<PORTB1);
		}
		else if(globalCount == 500000)
		{
			PORTB &= ~(1<<PORTB1);
			globalCount = 0;
		}
		
//		if(spiRequest == 1)
//		{
//			spiRequest = 0;
//			processSPI();
//		}
		get_reading();
	}
	return 0;
}
*/

uint8_t get_reading(void)
{
	if(!TWI_Transceiver_Busy())
	{
		switch(range_status)
		{
			case RANGE_INIT:
				// First byte is slave address indicating a write
				TWI_transBuff[0] = SLAVE_ADDR | TWI_WRITE;
				
				// Indicate write to SYSRANGE_START (0x18)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x18;
				
				// Write 0x01 to SYSRANGE_START to begin range measurement
				TWI_transBuff[3] = 0x01;
				TWI_Start_Transceiver_With_Data(TWI_transBuff,4);
				range_status = RANGE_READY_REQUEST;
				return FALSE;
				
			case RANGE_READY_REQUEST:
				// First byte is slave address indicating a write
				TWI_transBuff[0] = SLAVE_ADDR | TWI_WRITE;
				
				// Indicate read from RESULT_INTERRUPT_STATUS (0x4F)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x4F;
				
				TWI_Start_Transceiver_With_Data(TWI_transBuff,3);
				range_status = RANGE_READY_READ;
				
				return FALSE;
				
			case RANGE_READY_READ:
				/* Tell slave to send data */
				TWI_recBuff[0] = SLAVE_ADDR | TWI_READ;

				TWI_Start_Transceiver_With_Data(TWI_recBuff,2);
				range_status = RANGE_READY_GET;
				return FALSE;
			
			case RANGE_READY_GET:
				// Get data from slave
				TWI_Get_Data_From_Transceiver(TWI_recBuff);
				
				uint8_t ready = (TWI_recBuff[1] >> 2) & 0x01;
				
				// Check if measurement is done
				if(ready)
				{
					range_status = RANGE_DATA_REQUEST;
				}
				else
				{
					range_status = RANGE_READY_REQUEST;
				}
				return FALSE;
			
			case RANGE_DATA_REQUEST:
				// First byte is slave address indicating a write
				TWI_transBuff[0] = SLAVE_ADDR | TWI_WRITE;
				
				// Indicate read from RESULT_RANGE_VAL (0x62)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x62;
				
				TWI_Start_Transceiver_With_Data(TWI_transBuff,3);
				range_status = RANGE_DATA_READ;
				
				return FALSE;
			
			case RANGE_DATA_READ:
				/* Tell slave to send data */
				TWI_recBuff[0] = SLAVE_ADDR | TWI_READ;

				TWI_Start_Transceiver_With_Data(TWI_recBuff,2);
				range_status = RANGE_DATA_GET;
				
				return FALSE;
				
			case RANGE_DATA_GET:
				TWI_Get_Data_From_Transceiver(TWI_recBuff);
				
				// Store the distance measurement
				distance = TWI_recBuff[1];
				
				range_status = RANGE_RESET_INIT;
				return FALSE;
			
			case RANGE_RESET_INIT:
				// First byte is slave address indicating a write
				TWI_transBuff[0] = SLAVE_ADDR | TWI_WRITE;
				
				// Indicate write to SYSTEM_INTERRUPT_CLEAR (0x15)
				TWI_transBuff[1] = 0x0;
				TWI_transBuff[2] = 0x15;
				
				// Write 0x07 to SYSTEM_INTERRUPT_CLEAR to reset
				TWI_transBuff[3] = 0x07;
				TWI_Start_Transceiver_With_Data(TWI_transBuff,4);
				range_status = RANGE_READY_REQUEST;
				return FALSE;
			
			case RANGE_RESET_REQUEST:
				// First byte is slave address indicating a write
				TWI_transBuff[0] = SLAVE_ADDR | TWI_WRITE;
				
				// Indicate read from RESULT_RANGE_STATUS (0x4D)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x4D;
				
				TWI_Start_Transceiver_With_Data(TWI_transBuff,3);
				range_status = RANGE_RESET_READ;
				
				return FALSE;
				
			case RANGE_RESET_READ:
				/* Tell slave to send data */
				TWI_recBuff[0] = SLAVE_ADDR | TWI_READ;

				TWI_Start_Transceiver_With_Data(TWI_recBuff,2);
				range_status = RANGE_RESET_GET;
				
				return FALSE;
				
			case RANGE_RESET_GET:
				TWI_Get_Data_From_Transceiver(TWI_recBuff);
				
				// Check if it has been reset
				uint8_t reset = TWI_recBuff[1] & 0x1;
				if(reset)
				{
					range_status = RANGE_INIT;
					return TRUE;
				}
				else
				{
					range_status = RANGE_RESET_REQUEST;
					return FALSE;
				}				
			default:
				return FALSE;
				
		}	
		
	}
	
	return FALSE;
}

void spi_setup(void) {
	
	/**** EXTERNAL INTERRUPT *****/
	DDRD &= ~(1<<DDRD2); //setup External interrupt 0 as Input (PD2) (clear bit 2)
	PORTD |= (1<<PD2); //set PD2(INT0) to high (impedance) (pull-up resistor is activated)
	EIMSK |= (1<<INT0);  //enable INT0
	EICRA |= (1<<ISC01); //INT0 triggered on falling edge

	/********** SPI ***********/
	DDRB |= (1<<DDRB4); // MISO output
	SPCR = (1<<SPE); // Enable SPI
}

void processSPI(void) {
	uint8_t msb = SPDR;
	
	if(TEST)
	{
		SPDR = test1;
		if(test1 >= 255)
		{
			test1 = 0;
		}
		else
		{
			test1++;
		}
	}
	else
	{
		SPDR = distance;
	}
	
	
	
	msb = spi_slave_receive();
}

uint8_t spi_slave_receive(void) {
	// Wait for reception complete (until SPIF is set)
	while(!(SPSR & (1<<SPIF)));
	// Return Data Register
	return SPDR;
}

ISR(INT0_vect) {

	spiRequest = 1;
}