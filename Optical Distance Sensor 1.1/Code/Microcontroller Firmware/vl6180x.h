#ifndef VL6180X_h
#define VL6180X_h

typedef struct
{
	uint8_t addr; // The slave address of the sensor
	uint8_t transmit[10]; // Buffer for transmiting data over I2C
	uint8_t receive[10]; // Buffer for receiving data over I2C
	uint8_t distance; // The most recent distance measurement
	uint16_t als; // The most recent ambient light measurement
	int8_t port_B_pin; // The pin used as GPIO0 on PORTB. -1 if not used
	int8_t port_C_pin; // The pin used as GPIO0 on PORTC. -1 if not ued
	int8_t port_D_pin; // The pin used as GRPIO in PORTD. -1 if not used
}vl6180x;

// Initialize the sensor
uint8_t vl6180x_init(vl6180x * vl6Ptr, uint8_t address);

// Change the address of the sensor (identified by which pin on the microcontroller controls it)
uint8_t change_address(vl6180x * vl6Ptr, uint8_t new_addr);

// Get data from vl6Ptr. Set range_meas to 1 to get a range measurement, 0 for ALS measurement
// Returns 0 if the new and previous measurement were equal
uint8_t get_data(vl6180x * vl6Ptr, uint8_t range_meas);

// Initialize interleave mode
void start_interleave(vl6180x * vl6Ptr); 
	

#endif