#!/usr/bin/env python
# Derived from pub_optical.py (By Liang-Ting Jiang)
# Publishes data recieved by gripper

import rospy
from std_msgs.msg import ByteMultiArray
from optical_dist.msg import OpticalDist

data = OpticalDist()
ts = [0,0,0,0]
seq = 0

rospy.init_node("optical_distance_publisher", anonymous=True)
side = rospy.get_param('~side', 'right')
rospy.loginfo("Started publisher for the %s gripper", side)
if side == 'right':
	sub_topic = 'raw_pressure/r_gripper_motor'
elif side == 'left':
	sub_topic = 'raw_pressure/l_gripper_motor'
else:
	print side + ' is not a valid side parameter'
	raise

pub_topic = 'optical/' + side
pub = rospy.Publisher(pub_topic, OpticalDist)

def rawDataCallback(msg):
	global data,seq,ts
	if msg.data[:4] != ts:
		ts = msg.data[:4]
		seq += 1
		data.header.seq = seq
		data.header.stamp = rospy.Time.now()
		val1 = (256+msg.data[4]) if msg.data[4]<0 else msg.data[4]
		val2 = (256+msg.data[5]) if msg.data[5]<0 else msg.data[5]
		val3 = (256+msg.data[6]) if msg.data[6]<0 else msg.data[6]
		val4 = (256+msg.data[7]) if msg.data[7]<0 else msg.data[7]

		print "Data = %s %s %s %s" % (str(val1), str(val2), str(val3), str(val4))
		data.dist = [val1, val2, val3, val4]
		pub.publish(data)
sub = rospy.Subscriber(sub_topic, ByteMultiArray, rawDataCallback)
rospy.spin()
