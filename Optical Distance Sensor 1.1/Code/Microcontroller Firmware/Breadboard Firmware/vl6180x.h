#ifndef VL6180X_h
#define VL6180X_h

	typedef struct
	{
		uint8_t addr;
		uint8_t transmit[10];
		uint8_t receive[10];
		uint8_t distance;
		uint16_t als;
		uint8_t port_D_pin;
	}vl6180x;

	uint8_t vl6180x_init(vl6180x * vl6Ptr, uint8_t address);
	uint8_t change_address(vl6180x * vl6Ptr, uint8_t new_addr);
	uint8_t get_interleave(vl6180x * vl6Ptr);
	uint8_t get_data(vl6180x * vl6Ptr, uint8_t range_meas);
	uint8_t readRange(vl6180x * vl6Ptr);
	

#endif