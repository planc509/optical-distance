#ifndef I2C_h

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define I2C_h

// vl6Ptr: Must have transmit buffer loaded with the following
//		slave address at index 0
//		register address to read from at indices 1 and 2
// It must also have the receive buffer loaded with the (read) slave address at the first index
// Will have data in subsequent bit after read
// n: The number of bytes to read from vl6Ptr
uint8_t read_bytes(vl6180x * vl6Ptr, uint8_t n);

// vl6Ptr: Must have transmit buffer loaded with the following
//		slave address at index 0
//		register address to write to at indices 1 and 2
//		data at subsequent indices
// n: The number of bytes to write to vl6Ptr (including slave and reg address)
uint8_t write_bytes(vl6180x * vl6Ptr, uint8_t n);

// Write data to the reg_addr of the sensor corresponding to vl6Ptr
// vl6Ptr must have correct slave address in addr field
uint8_t write_byte(vl6180x * vl6Ptr, uint16_t reg_addr, uint8_t data);

// Initialize I2C
void TWI_Master_Initialise(void);

// Helper method for I2C
uint8_t TWI_Poll(uint8_t cmd);
#endif