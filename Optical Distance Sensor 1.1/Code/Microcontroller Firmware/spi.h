#ifndef SPI_H
#define SPI_H

// Setup SPI
void spi_setup(void);

// Responds to the masters request for data. Transmits back 5 values.
void processSPI(void);

// Quick method to check if the gripper wants data
void checkSPI(void);

#endif