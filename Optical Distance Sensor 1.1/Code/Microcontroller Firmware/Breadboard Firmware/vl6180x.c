#include <util/delay.h>
#include <avr/io.h>
#include "vl6180x.h"
#include "TWI_Master.h"
#include "i2c.h"

#ifndef DEF_SLAVE_ADDR
#define DEF_SLAVE_ADDR 0x52
#endif 

#ifndef TWI_READ
#define TWI_READ 0x1
#endif

#ifndef TWI_WRITE
#define TWI_WRITE 0x0
#endif

#ifndef INTER_LEAVE_INIT
#define INTER_LEAVE_INIT 0
#endif

#ifndef MEAS_INIT
#define MEAS_INIT 1
#endif

#ifndef MEAS_READY
#define MEAS_READY 2
#endif

#ifndef MEAS_DATA
#define MEAS_DATA 3
#endif

#ifndef MEAS_RESET_INIT
#define MEAS_RESET_INIT 4
#endif

#ifndef MEAS_RESET_READ
#define MEAS_RESET_READ 5
#endif

extern uint8_t meas_status;

uint8_t test;

uint8_t vl6180x_init(vl6180x * vl6Ptr, uint8_t address) {
	char reset;
	for(int i = 0; i < 10; i++)
	{
		vl6Ptr->transmit[i] = 0;
		vl6Ptr->receive[i] = 0;
	}
	vl6Ptr->distance = 0;
	vl6Ptr->als = 0;
	test = change_address(vl6Ptr, address);
	// First byte is slave address indicating a write
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x16;
	
	/* Tell slave to send data */
	vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
	
	read_bytes(vl6Ptr,1);
	
	while(TWI_Transceiver_Busy());
	
	reset = vl6Ptr->receive[1];
	if (1==1){ // check to see has it been Initialised already
		
		// Mandatory : private registers
		write_byte(vl6Ptr,0x0207, 0x01);
		write_byte(vl6Ptr,0x0208, 0x01);
		write_byte(vl6Ptr,0x0096, 0x00);
		write_byte(vl6Ptr,0x0097, 0xfd);
		write_byte(vl6Ptr,0x00e3, 0x00);
		write_byte(vl6Ptr,0x00e4, 0x04);
		write_byte(vl6Ptr,0x00e5, 0x02);
		write_byte(vl6Ptr,0x00e6, 0x01);
		write_byte(vl6Ptr,0x00e7, 0x03);
		write_byte(vl6Ptr,0x00f5, 0x02);
		write_byte(vl6Ptr,0x00d9, 0x05);
		write_byte(vl6Ptr,0x00db, 0xce);
		write_byte(vl6Ptr,0x00dc, 0x03);
		write_byte(vl6Ptr,0x00dd, 0xf8);
		write_byte(vl6Ptr,0x009f, 0x00);
		write_byte(vl6Ptr,0x00a3, 0x3c);
		write_byte(vl6Ptr,0x00b7, 0x00);
		write_byte(vl6Ptr,0x00bb, 0x3c);
		write_byte(vl6Ptr,0x00b2, 0x09);
		write_byte(vl6Ptr,0x00ca, 0x09);
		write_byte(vl6Ptr,0x0198, 0x01);
		write_byte(vl6Ptr,0x01b0, 0x17);
		write_byte(vl6Ptr,0x01ad, 0x00);
		write_byte(vl6Ptr,0x00ff, 0x05);
		write_byte(vl6Ptr,0x0100, 0x05);
		write_byte(vl6Ptr,0x0199, 0x05);
		write_byte(vl6Ptr,0x01a6, 0x1b);
		write_byte(vl6Ptr,0x01ac, 0x3e);
		write_byte(vl6Ptr,0x01a7, 0x1f);
		write_byte(vl6Ptr,0x0030, 0x00);
		
		//Recommended : Public registers - See data sheet for more detail
		write_byte(vl6Ptr,0x0011, 0x10); // Enables polling for �New Sample ready� when measurement completes
		write_byte(vl6Ptr,0x010a, 0x30); // Set the averaging sample period (compromise between lower noise and increased execution time)
		write_byte(vl6Ptr,0x003f, 0x46); // Sets the light and dark gain (upper nibble). Dark gain should not be changed.
		write_byte(vl6Ptr,0x0031, 0xFF); // sets the # of range measurements after which auto calibration of system is performed
		write_byte(vl6Ptr,0x0040, 0x63); // Set ALS integration time to 100msDocID026571 Rev 1 25/27
//		write_byte(vl6Ptr,0x0040, 0x32); // Set ALS integration time to 50ms
		write_byte(vl6Ptr,0x002e, 0x01); // perform a single temperature calibration of the ranging sensor
		write_byte(vl6Ptr,0x001b, 0x09); // Set default ranging inter-measurement period to 100ms
		write_byte(vl6Ptr,0x003e, 0x31); // Set default ALS inter-measurement period to 500ms
//		write_byte(vl6Ptr,0x003e, 0x0A); // Set default ALS inter-measurement period to 100ms
		write_byte(vl6Ptr,0x0014, 0x24); // Configures interrupt on �New Sample Ready threshold event�
		write_byte(vl6Ptr,0x001c, 0x31); // Set Max Range convergence time
//		write_byte(vl6Ptr,0x001c, 0x1e); // Set Max Range convergence time to 30ms
//		write_byte(vl6Ptr,0x003f, 0x47); // Set the ALS analog gain to 40

		write_byte(vl6Ptr,0x002D,0x11);

		write_byte(vl6Ptr,0x0016, 0x00); // No longer fresh out of reset
	}
	
	// First byte is slave address indicating a write
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
		
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x16;
		
	/* Tell slave to send data */
	vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
		
	read_bytes(vl6Ptr,1);
	
	while(TWI_Transceiver_Busy());
	
	return vl6Ptr->addr;
}

uint8_t change_address(vl6180x * vl6Ptr, uint8_t new_addr)
{
	vl6Ptr->transmit[0] = new_addr | TWI_WRITE;
	vl6Ptr->transmit[1] = 0x02;
	vl6Ptr->transmit[2] = 0x12;
	
	/* Tell slave to send data */
	vl6Ptr->receive[0] = new_addr | TWI_READ;
	
	read_bytes(vl6Ptr,1);
	
	while(TWI_Transceiver_Busy());
	
	vl6Ptr->addr = (vl6Ptr->receive[1]) << 1;
	if(vl6Ptr->addr == new_addr)
	{
		return vl6Ptr->addr;
	}
	vl6Ptr->addr = DEF_SLAVE_ADDR |TWI_WRITE;
	write_byte(vl6Ptr, 0x0212, new_addr >> 1);
	
	while(TWI_Transceiver_Busy());
	vl6Ptr->transmit[0] = new_addr | TWI_WRITE;
	vl6Ptr->transmit[1] = 0x02;
	vl6Ptr->transmit[2] = 0x12;
	
	/* Tell slave to send data */
	vl6Ptr->receive[0] = new_addr | TWI_READ;
	
	read_bytes(vl6Ptr,1);
	
	vl6Ptr->addr = (vl6Ptr->receive[1]) << 1;
	
	while(TWI_Transceiver_Busy());
	
	vl6Ptr->transmit[0] = new_addr | TWI_WRITE;
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x00;
		
	/* Tell slave to send data */
	vl6Ptr->receive[0] = new_addr | TWI_READ;
		
	read_bytes(vl6Ptr,1);
	
	while(TWI_Transceiver_Busy());
	
	return vl6Ptr->addr;
	
}

uint8_t readRange(vl6180x * vl6Ptr)
{
	// First byte is slave address indicating a write
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	
	// Indicate read from 0x4d
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x4D;
	
	/* Tell slave to send data */
	vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
	
	read_bytes(vl6Ptr,1);
	
	if(1==1)//vl6Ptr->receive[1] == 1 || vl6Ptr->receive[1] == 2 || vl6Ptr->receive[1] == 3) 
	{
		// First byte is slave address indicating a write
		vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
		// Indicate write to INTER_LEAVED_MODE_ENABLE (0x02A3)
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x18;
			
		// Activate Interleaved mode
		vl6Ptr->transmit[3] = 0x01;
			
		write_bytes(vl6Ptr,4);
		
		while(1)
		{
			while(TWI_Transceiver_Busy());
			// First byte is slave address indicating a write
			vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
			// Indicate read from 0x4d
			vl6Ptr->transmit[1] = 0x00;
			vl6Ptr->transmit[2] = 0x4f;
			
			/* Tell slave to send data */
			vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
			
			read_bytes(vl6Ptr,1);
			while(TWI_Transceiver_Busy());
			if(vl6Ptr->receive[1] == 4)
			{
				break;
			}
			else
			{
				// First byte is slave address indicating a write
				vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	
				// Indicate read from 0x4d
				vl6Ptr->transmit[1] = 0x00;
				vl6Ptr->transmit[2] = 0x4D;
	
				/* Tell slave to send data */
				vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
	
				read_bytes(vl6Ptr,1);
				while(TWI_Transceiver_Busy());
				if(vl6Ptr->receive[1] == 1)
				{
					// First byte is slave address indicating a write
					vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
				
					// Indicate read from 0x4d
					vl6Ptr->transmit[1] = 0x00;
					vl6Ptr->transmit[2] = 0x18;
				
					/* Tell slave to send data */
					vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
				
					read_bytes(vl6Ptr,1);
					while(TWI_Transceiver_Busy());
				}
			}
		}
		
		while(TWI_Transceiver_Busy());
		// First byte is slave address indicating a write
		vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
					
		// Indicate read from 0x4d
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x62;
					
		/* Tell slave to send data */
		vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
					
		read_bytes(vl6Ptr,1);
		
		return TRUE;
	}
}

uint8_t get_interleave(vl6180x * vl6Ptr)
{
	static uint8_t range_meas = 0;

	if(!TWI_Transceiver_Busy())
	{
		switch(meas_status)
		{
			case INTER_LEAVE_INIT:
			
			// First byte is slave address indicating a write
			vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
			// Indicate write to INTER_LEAVED_MODE_ENABLE (0x02A3)
			vl6Ptr->transmit[1] = 0x02;
			vl6Ptr->transmit[2] = 0xA3;
			
			// Activate Interleaved mode
			vl6Ptr->transmit[3] = 0x01;
			
			write_bytes(vl6Ptr,4);
			
			meas_status = MEAS_INIT;
			return FALSE;
			
			case MEAS_INIT:
			// First byte is slave address indicating a write
			vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
			// Indicate write to SYSALS_START (0x18)
			vl6Ptr->transmit[1] = 0x00;
			vl6Ptr->transmit[2] = 0x38;
			
			
			// Write 0x01  to begin range measurement
			//TWI_transBuff[3] = 0x01;
			// Write 0x03 to SYSRANGE_START for continuous measurements
			vl6Ptr->transmit[3] = 0x03;
			write_bytes(vl6Ptr,4);
			meas_status = MEAS_READY;
			return FALSE;
			
			case MEAS_READY:
			// First byte is slave address indicating a write
			vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
			// Indicate read from RESULT_INTERRUPT_STATUS (0x4F)
			vl6Ptr->transmit[1] = 0x00;
			vl6Ptr->transmit[2] = 0x4F;
			
			/* Tell slave to send data */
			vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
			
			read_bytes(vl6Ptr,1);
			
			uint8_t ready = 0;
			
			if(range_meas)
			{
				ready = (vl6Ptr->receive[1] >> 2) & 0x01;
				
			}
			else
			{
				ready = (vl6Ptr->receive[1] >> 5) & 0x01;
			}
			
			
			// Check if measurement is done
			if(ready)
			{
				meas_status = MEAS_DATA;
				
			}

			return FALSE;
			
			case MEAS_DATA:
			// First byte is slave address indicating a write
			vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
			if(range_meas)
			{
				// Indicate read from RESULT_RANGE_VAL (0x62)
				vl6Ptr->transmit[1] = 0x00;
				vl6Ptr->transmit[2] = 0x62;
				
				/* Tell slave to send data */
				vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
				
				read_bytes(vl6Ptr,1);
				
				vl6Ptr->distance = vl6Ptr->receive[1];
			}
			else
			{
				// Indicate read from RESULT_RANGE_VAL (0x62)
				vl6Ptr->transmit[1] = 0x00;
				vl6Ptr->transmit[2] = 0x50;
				
				/* Tell slave to send data */
				vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
				
				read_bytes(vl6Ptr,2);
				
				vl6Ptr->als = (vl6Ptr->receive[1] << 8) | vl6Ptr->receive[2];
			}

			meas_status = MEAS_RESET_INIT;
			
			return FALSE;
			
			case MEAS_RESET_INIT:
			
			// First byte is slave address indicating a write
			vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
			// Indicate write to SYSTEM_INTERRUPT_CLEAR (0x15)
			vl6Ptr->transmit[1] = 0x0;
			vl6Ptr->transmit[2] = 0x15;
			
			// Write 0x07 to SYSTEM_INTERRUPT_CLEAR to reset
			if(range_meas)
			{
				vl6Ptr->transmit[3] = 0x01;
			}
			else
			{
				vl6Ptr->transmit[3] = 0x02;
			}
			
			write_bytes(vl6Ptr,4);
			
			meas_status = MEAS_READY;
			
			range_meas = (range_meas + 1) % 2;
			
			return TRUE;
			
			case MEAS_RESET_READ:
			
			// First byte is slave address indicating a write
			vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
			
			if(range_meas)
			{
				// Indicate read from RESULT_RANGE_STATUS (0x4D)
				vl6Ptr->transmit[1] = 0x00;
				vl6Ptr->transmit[2] = 0x4D;
			}
			else
			{
				// Indicate read from RESULT_ALS_STATUS (0x4D)
				vl6Ptr->transmit[1] = 0x00;
				vl6Ptr->transmit[2] = 0x4E;
			}

			
			/* Tell slave to send data */
			vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
			
			read_bytes(vl6Ptr,1);
			
			// Check if it has been reset
			uint8_t reset = vl6Ptr->receive[1] & 0x1;
			if(reset)
			{
				meas_status = MEAS_INIT;
			}
			else
			{
				meas_status = MEAS_RESET_READ;
			}
			
			return FALSE;
			
			default:
			return FALSE;
			
		}
		
	}
	
	return FALSE;
}

uint8_t get_data(vl6180x * vl6Ptr, uint8_t range_meas)
{
	// First byte is slave address indicating a write
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	uint16_t old_meas = 0;		
	if(range_meas)
	{
		// Indicate read from RESULT_RANGE_VAL (0x62)
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x62;
					
		/* Tell slave to send data */
		vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
					
		read_bytes(vl6Ptr,1);
		old_meas = vl6Ptr->distance;
		vl6Ptr->distance = vl6Ptr->receive[1];
		return vl6Ptr->distance != old_meas;
		
	}
	else
	{
		// Indicate read from RESULT_RANGE_VAL (0x62)
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x50;
					
		/* Tell slave to send data */
		vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
					
		read_bytes(vl6Ptr,2);
		old_meas = vl6Ptr->als;
		vl6Ptr->als = (vl6Ptr->receive[1] << 8) | vl6Ptr->receive[2];
		return vl6Ptr->als != old_meas;
	}
				
}

/*
uint8_t get_reading(uint8_t range_meas)
{
	if(!TWI_Transceiver_Busy())
	{
		switch(meas_status)
		{
			case MEAS_INIT:
			// First byte is slave address indicating a write
			TWI_transBuff[0] = DEF_SLAVE_ADDR | TWI_WRITE;
			
			if(range_meas)
			{
				// Indicate write to SYSRANGE_START (0x18)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x18;
			}
			else
			{
				// Indicate write to SYSALS_START (0x18)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x38;
			}
			
			
			// Write 0x01  to begin range measurement
			//TWI_transBuff[3] = 0x01;
			// Write 0x03 to SYSRANGE_START for continuous measurements
			TWI_transBuff[3] = 0x03;
			write_bytes(TWI_transBuff,4);
			meas_status = MEAS_READY;
			return FALSE;
			
			case MEAS_READY:
			// First byte is slave address indicating a write
			TWI_transBuff[0] = DEF_SLAVE_ADDR | TWI_WRITE;
			
			// Indicate read from RESULT_INTERRUPT_STATUS (0x4F)
			TWI_transBuff[1] = 0x00;
			TWI_transBuff[2] = 0x4F;
			
			// Tell slave to send data 
			TWI_recBuff[0] = DEF_SLAVE_ADDR | TWI_READ;
			
			read_bytes(TWI_transBuff, TWI_recBuff,1);
			
			uint8_t ready = 0;
			
			if(range_meas)
			{
				ready = (TWI_recBuff[1] >> 2) & 0x01;
			}
			else
			{
				ready = (TWI_recBuff[1] >> 5) & 0x01;
			}
			
			
			// Check if measurement is done
			if(ready)
			{
				meas_status = MEAS_DATA;
			}

			return FALSE;
			
			case MEAS_DATA:
			// First byte is slave address indicating a write
			TWI_transBuff[0] = DEF_SLAVE_ADDR | TWI_WRITE;
			
			if(range_meas)
			{
				// Indicate read from RESULT_RANGE_VAL (0x62)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x62;
				
				// Tell slave to send data 
				TWI_recBuff[0] = DEF_SLAVE_ADDR | TWI_READ;
				
				read_bytes(TWI_transBuff,TWI_recBuff,1);
				
				distance = TWI_recBuff[1];
			}
			else
			{
				// Indicate read from RESULT_RANGE_VAL (0x62)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x50;
				
				// Tell slave to send data 
				TWI_recBuff[0] = DEF_SLAVE_ADDR | TWI_READ;
				
				read_bytes(TWI_transBuff,TWI_recBuff,2);
				
				als = (TWI_recBuff[1] << 8) | TWI_recBuff[2];
			}

			
			
			
			meas_status = MEAS_RESET_INIT;
			
			return FALSE;
			
			case MEAS_RESET_INIT:
			
			// First byte is slave address indicating a write
			TWI_transBuff[0] = DEF_SLAVE_ADDR | TWI_WRITE;
			
			// Indicate write to SYSTEM_INTERRUPT_CLEAR (0x15)
			TWI_transBuff[1] = 0x0;
			TWI_transBuff[2] = 0x15;
			
			// Write 0x07 to SYSTEM_INTERRUPT_CLEAR to reset
			TWI_transBuff[3] = 0x07;
			write_bytes(TWI_transBuff,4);
			
			meas_status = MEAS_READY;
			
			return TRUE;
			
			case MEAS_RESET_READ:
			
			// First byte is slave address indicating a write
			TWI_transBuff[0] = DEF_SLAVE_ADDR | TWI_WRITE;
			
			if(range_meas)
			{
				// Indicate read from RESULT_RANGE_STATUS (0x4D)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x4D;
			}
			else
			{
				// Indicate read from RESULT_ALS_STATUS (0x4D)
				TWI_transBuff[1] = 0x00;
				TWI_transBuff[2] = 0x4E;
			}

			
			// Tell slave to send data 
			TWI_recBuff[0] = DEF_SLAVE_ADDR | TWI_READ;
			
			read_bytes(TWI_transBuff,TWI_recBuff,1);
			
			// Check if it has been reset
			uint8_t reset = TWI_recBuff[1] & 0x1;
			if(reset)
			{
				meas_status = MEAS_INIT;
			}
			else
			{
				meas_status = MEAS_RESET_READ;
			}
			
			return FALSE;
			
			default:
			return FALSE;
			
		}
		
	}
	
	return FALSE;
} */