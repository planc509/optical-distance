/*
 * OpticalDistance.c
 *
 * Created: 11/17/2014
 *  Author: Patrick
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "TWI_Master.h"
#include "vl6180x.h"

#define TEST FALSE

#ifndef DEF_SLAVE_ADDR
#define DEF_SLAVE_ADDR 0x52
#endif

#ifndef TWI_READ
#define TWI_READ 0x1
#endif
#ifndef TWI_WRITE
#define TWI_WRITE 0x0
#endif


#ifndef INTER_LEAVE_INIT
#define INTER_LEAVE_INIT 0
#endif
#ifndef MEAS_INIT
#define MEAS_INIT 1
#endif
#ifndef MEAS_READY
#define MEAS_READY 2
#endif
#ifndef MEAS_DATA
#define MEAS_DATA 3
#endif
#ifndef MEAS_RESET_INIT
#define MEAS_RESET_INIT 4
#endif
#ifndef MEAS_RESET_READ
#define MEAS_RESET_READ 5
#endif

#ifndef ALS_MEAS
#define ALS_MEAS 0
#endif
#ifndef RANGE_MEAS
#define RANGE_MEAS 1
#endif

uint8_t test1;

uint8_t spiRequest;

uint8_t meas_status;
uint8_t meas_count;
vl6180x * vl6_queue[10];
uint8_t vl6_count;
uint8_t spi_vl6_idx;
uint32_t global_count;
void processSPI(void);
void spi_setup(void);
uint8_t spi_slave_receive(void);


int main(void) {
	DDRD &= ~(1<<DDRD1);
	PORTD &= ~(PORTD1);
	test1 = 0;
	global_count = 0;
//	spi_setup();
	TWI_Master_Initialise();
	meas_count = 0;
	spi_vl6_idx = 0;
	sei();     // Enable interrupts
	
	DDRB |= (1<<DDRB1);
//	DDRB &= ~(1<<DDRB0);
	PORTB |= (1<<PORTB1);
	
	meas_status = INTER_LEAVE_INIT;
	
	vl6_count = 2;
	
	vl6180x bottom;
	bottom.port_D_pin = 6;
	DDRD |= (1<<DDRD6);
	
	vl6180x left;
	left.port_D_pin = 7;
	DDRD |= (1<<DDRD7);
	
	
	vl6_queue[0] = &bottom;	
	vl6_queue[1] = &left;

	uint8_t result = 0;
	// TODO: Pull all GPIO0 low
	for(int i = 0; i < vl6_count; i++)
	{
		PORTD &= ~(1<<(vl6_queue[i]->port_D_pin));
	}
	_delay_ms(2);
	for(int i = 0; i < vl6_count; i++)
	{
		// TODO: Pull corresponding GPIO0 high
		PORTD |= (1<<(vl6_queue[i]->port_D_pin));
		_delay_ms(2);
		vl6180x_init(vl6_queue[i], DEF_SLAVE_ADDR + 2*(i+1));
		int j = 0;
		meas_status = INTER_LEAVE_INIT;
		while(j < 2)
		{
	//		result = readRange(vl6_queue[i]);
			result = get_interleave(vl6_queue[i]);
			if(result == TRUE)
			{
				j++;
			}
		}
	}
	
	while(1) {
		
		for(int i = 0; i < vl6_count; i++)
		{
			
			for(int j = 0; j < 2; j++)
			{
//				result = readRange(vl6_queue[i]);
				result = get_data(vl6_queue[i],j);
			}
						
		} 
		global_count++;
		if(global_count > 1000)
		{
			PORTB ^= (1<<PB1);
			global_count = 0;
		}
		
	}
	return 0;
}

void spi_setup(void) {
	
	/**** EXTERNAL INTERRUPT *****/
	DDRD &= ~(1<<DDRD2); //setup External interrupt 0 as Input (PD2) (clear bit 2)
	PORTD |= (1<<PD2); //set PD2(INT0) to high (impedance) (pull-up resistor is activated)
	EIMSK |= (1<<INT0);  //enable INT0
	EICRA |= (1<<ISC01); //INT0 triggered on falling edge

	/********** SPI ***********/
	DDRB |= (1<<DDRB4); // MISO output
	SPCR = (1<<SPE); // Enable SPI
}

void processSPI(void) {
	uint8_t msb = SPDR;
	
	if(TEST)
	{
		SPDR = test1;
		if(test1 >= 255)
		{
			test1 = 0;
		}
		else
		{
			test1++;
		}
	}
	else
	{
		
		if(meas_count == 0)
		{
			SPDR = vl6_queue[spi_vl6_idx]->distance;
		}
		else if(meas_count == 1)
		{
			SPDR = (vl6_queue[spi_vl6_idx]->als)>>8;
		}
		else
		{
			SPDR = 0x00FF & (vl6_queue[spi_vl6_idx]->als);
			spi_vl6_idx = (spi_vl6_idx + 1) % vl6_count;
		}
		
		meas_count = (meas_count + 1) % 3;
		
	}
	
	
	
	msb = spi_slave_receive();
	
}

uint8_t spi_slave_receive(void) {
	// Wait for reception complete (until SPIF is set)
	while(!(SPSR & (1<<SPIF)));
	// Return Data Register
	return SPDR;
}

ISR(INT0_vect) {
//	cli();
	processSPI();
//	sei();
}