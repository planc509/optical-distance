#ifndef VL6180X_h
#define VL6180X_h
	int vl6180x_init(void);
	uint8_t get_reading(uint8_t range_meas);
	uint8_t get_interleave(void);
	
	typedef struct 
	{
		uint8_t addr;
		uint8_t transmit[10];
		uint8_t receive[10];
		uint8_t distance;
		uint8_t als;
	}vl6180x;
#endif