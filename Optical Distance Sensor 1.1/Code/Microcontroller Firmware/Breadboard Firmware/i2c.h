#ifndef I2C_h
#define I2C_h
	void read_bytes(vl6180x * vl6Ptr, uint8_t n);
	void write_bytes(vl6180x * vl6Ptr, uint8_t n);
	void write_byte(vl6180x * vl6Ptr, uint16_t reg_addr, uint8_t data);
#endif