#include <util/delay.h>
#include <avr/io.h>
#include "TWI_MASTER.h"
#include "vl6180x.h"
#include "i2c.h"


#ifndef TWI_READ
#define TWI_READ 0x1
#endif

#ifndef TWI_WRITE 
#define TWI_WRITE 0x0
#endif

// transmit: Holds the slave address and address of register to read from
// receive: Holds the slave address before, will hold slave address and data after
// n: The number of bytes to read
void read_bytes(vl6180x * vl6Ptr, uint8_t n)
{
	TWI_Start_Transceiver_With_Data(vl6Ptr->transmit,3);
	
	while(TWI_Transceiver_Busy());

	TWI_Start_Transceiver_With_Data(vl6Ptr->receive,n+1);
	
	while(TWI_Transceiver_Busy());
	
	TWI_Get_Data_From_Transceiver(vl6Ptr->receive);
}

// The number of bytes to write (not including address)
void write_bytes(vl6180x * vl6Ptr, uint8_t n)
{
	TWI_Start_Transceiver_With_Data(vl6Ptr->transmit,n + 1);
}

void write_byte(vl6180x * vl6Ptr, uint16_t reg_addr, uint8_t data)
{
	// First byte is slave address indicating a write
	vl6Ptr->transmit[0] = vl6Ptr->addr & ~(TWI_READ);
	
	// Load register to write to
	vl6Ptr->transmit[1] = reg_addr >> 8;
	vl6Ptr->transmit[2] = reg_addr & 0x00FF;
	
	// Load the data to be written
	vl6Ptr->transmit[3] = data;
	
	TWI_Start_Transceiver_With_Data(vl6Ptr->transmit,4);
}