/*
 * OpticalDistance.c
 *
 * Created: 11/17/2014
 *  Author: Patrick
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "TWI_Master.h"
#include "vl6180x.h"

#define TEST FALSE

#ifndef SLAVE_ADDR
#define SLAVE_ADDR 0x52
#endif

#ifndef TWI_READ
#define TWI_READ 0x1
#endif
#ifndef TWI_WRITE
#define TWI_WRITE 0x0
#endif


#ifndef INTER_LEAVE_INIT
#define INTER_LEAVE_INIT 0
#endif
#ifndef MEAS_INIT
#define MEAS_INIT 1
#endif
#ifndef MEAS_READY
#define MEAS_READY 2
#endif
#ifndef MEAS_DATA
#define MEAS_DATA 3
#endif
#ifndef MEAS_RESET_INIT
#define MEAS_RESET_INIT 4
#endif
#ifndef MEAS_RESET_READ
#define MEAS_RESET_READ 5
#endif

#ifndef ALS_MEAS
#define ALS_MEAS 0
#endif
#ifndef RANGE_MEAS
#define RANGE_MEAS 1
#endif

static uint32_t globalCount = 0;

uint16_t test1;
uint8_t spiRequest;
uint8_t TWI_transBuff[10], TWI_recBuff[10];
uint8_t meas_status;
uint8_t distance;
uint16_t als;
char success;
uint8_t meas_count;
uint8_t get_range_meas;

void processSPI(void);
void spi_setup(void);
uint8_t spi_slave_receive(void);
uint8_t get_reading(uint8_t range_meas);
void read_bytes(uint8_t * data, uint8_t * receive, uint8_t n);
void write_bytes(uint8_t * data, uint8_t n);
int vl6180x_init();
void write_byte(uint8_t slave_addr, uint16_t reg_addr, uint8_t data);

int main(void) {
	spi_setup();
	TWI_Master_Initialise();
	meas_count = 0;
	sei();     // Enable interrupts
	
	DDRB |= (1<<DDRB1);
	DDRB &= ~(1<<DDRB0);
	DDRD |= (1<<DDRD6) | (1<<DDRD7);
	PORTB &= ~(1<<PORTB1);
	PORTD |= (1<<PORTD6) | (1<<PORTD7);
	meas_status = INTER_LEAVE_INIT;
//	TWI_recBuff[1] = 1;
	distance = 0;
	get_range_meas = 0;
	als = 0;
	vl6180x_init();
	
	while(1) {
	//	globalCount++;
	//	if(globalCount == 2500)
	//	{
	//		PORTB |= (1<<PORTB1);
	//	}
	//	else if(globalCount == 5000)
	//	{
	//		PORTB &= ~(1<<PORTB1);
	//		globalCount = 0;
	//	}
		
//		if(spiRequest == 1)
//		{
//			spiRequest = 0;
//			processSPI();
//		}
		
		uint8_t result = get_interleave();
		if( result == TRUE)
		{
			get_range_meas = (get_range_meas + 1) % 2;
			if(get_range_meas == 0)
			{
				PORTB ^= (1<<PB1);
			}
		}
	}
	return 0;
}

void spi_setup(void) {
	
	/**** EXTERNAL INTERRUPT *****/
	DDRD &= ~(1<<DDRD2); //setup External interrupt 0 as Input (PD2) (clear bit 2)
	PORTD |= (1<<PD2); //set PD2(INT0) to high (impedance) (pull-up resistor is activated)
	EIMSK |= (1<<INT0);  //enable INT0
	EICRA |= (1<<ISC01); //INT0 triggered on falling edge

	/********** SPI ***********/
	DDRB |= (1<<DDRB4); // MISO output
	SPCR = (1<<SPE); // Enable SPI
}

void processSPI(void) {
	uint8_t msb = SPDR;
	
	if(TEST)
	{
		SPDR = test1;
		if(test1 >= 255)
		{
			test1 = 0;
		}
		else
		{
			test1++;
		}
	}
	else
	{
		
		if(meas_count == 0)
		{
			SPDR = distance;
		}
		else if(meas_count == 1)
		{
			SPDR = als >> 8;
		}
		else
		{
			SPDR = 0x00FF & als;
		}
		
		meas_count = (meas_count + 1) % 3;
		
	}
	
	
	
	msb = spi_slave_receive();
	
}

uint8_t spi_slave_receive(void) {
	// Wait for reception complete (until SPIF is set)
	while(!(SPSR & (1<<SPIF)));
	// Return Data Register
	return SPDR;
}

ISR(INT0_vect) {
//	cli();
	processSPI();
//	sei();
}