/*
 * OpticalDistance.c
 *
 * Created: 11/17/2014
 *  Author: Patrick
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include "vl6180x.h"
#include "i2c.h"
#include "spi.h"

#ifndef DEF_SLAVE_ADDR
#define DEF_SLAVE_ADDR 0x52
#endif

#ifndef TWI_READ
#define TWI_READ 0x1
#endif
#ifndef TWI_WRITE
#define TWI_WRITE 0x0
#endif

#ifndef ALS_MEAS
#define ALS_MEAS 0
#endif
#ifndef RANGE_MEAS
#define RANGE_MEAS 1
#endif

const uint8_t vl6_count = 6;

vl6180x * vl6_queue[10];
uint32_t global_count;
uint8_t spiFlag;

int main(void) {
	// Used for LED timing
	global_count = 0;
	
	// Indicates that gripper has requested data
	spiFlag = 0;
//	wdt_disable();
//	wdt_enable(WDTO_8S);
	
	spi_setup();
	sei();     // Enable interrupts
	
	// Output for LED
	DDRB |= (1<<DDRB1);
	
	// Initialize each sensor's data structure
	vl6180x center;
	center.port_B_pin = -1;
	center.port_C_pin = 1;
	center.port_D_pin = -1;
	DDRC |= (1<<center.port_C_pin);
/*	
	vl6180x left_back_flat;
	left_back_flat.port_B_pin = -1;
	left_back_flat.port_C_pin = -1;
	left_back_flat.port_D_pin = 0;
	DDRD |= (1<<left_back_flat.port_D_pin);
	
	vl6180x left_front_flat;
	left_front_flat.port_B_pin = -1;
	left_front_flat.port_C_pin = -1;
	left_front_flat.port_D_pin = 6;
	DDRD |= (1<<left_front_flat.port_D_pin);
	
	vl6180x right_back_flat;
	right_back_flat.port_B_pin = -1;
	right_back_flat.port_C_pin = -1;
	right_back_flat.port_D_pin = 4;
	DDRD |= (1<<right_back_flat.port_D_pin);
	
	vl6180x right_front_flat;
	right_front_flat.port_B_pin = -1;
	right_front_flat.port_C_pin = 3;
	right_front_flat.port_D_pin = -1;
	DDRC |= (1<<right_front_flat.port_C_pin);
	*/
	vl6180x left_back_lateral;
	left_back_lateral.port_B_pin = -1;
	left_back_lateral.port_C_pin = -1;
	left_back_lateral.port_D_pin = 3;
	DDRD |= (1<<left_back_lateral.port_D_pin);
	
	vl6180x left_front_lateral;
	left_front_lateral.port_B_pin = -1;
	left_front_lateral.port_C_pin = -1;
	left_front_lateral.port_D_pin = 1;
	DDRD |= (1<<left_front_lateral.port_D_pin);
	
	vl6180x front_tip;
	front_tip.port_B_pin = -1;
	front_tip.port_C_pin = 0;
	front_tip.port_D_pin = -1;
	DDRC |= (1<<front_tip.port_C_pin);
	
	vl6180x right_back_lateral;
	right_back_lateral.port_B_pin = 0;
	right_back_lateral.port_C_pin = -1;
	right_back_lateral.port_D_pin = -1;
	DDRB |= (1<<right_back_lateral.port_B_pin); 
	
	vl6180x right_front_lateral;
	right_front_lateral.port_B_pin = -1;
	right_front_lateral.port_C_pin = 2;
	right_front_lateral.port_D_pin = -1;
	DDRC |= (1<<right_front_lateral.port_C_pin);

	// Place each sensor in queue
	vl6_queue[0] = &center;
/*	vl6_queue[0] = &left_back_flat;
	vl6_queue[1] = &left_front_flat;
	vl6_queue[2] = &right_back_flat;
	vl6_queue[3] = &right_front_flat; */
	vl6_queue[1] = &left_back_lateral;
	vl6_queue[2] = &left_front_lateral;
	vl6_queue[3] = &front_tip;
	vl6_queue[4] = &right_back_lateral;
	vl6_queue[5] = &right_front_lateral; 

	uint8_t result = 0;
	// Pull all GPIO0 low
	for(int i = 0; i < vl6_count; i++) {
		if(vl6_queue[i]->port_B_pin >= 0) {
			PORTB &= ~(1<<(vl6_queue[i]->port_B_pin));
		} else if(vl6_queue[i]->port_C_pin >= 0) {
			PORTC &= ~(1<<(vl6_queue[i]->port_C_pin));
		} else if(vl6_queue[i]->port_D_pin >= 0) {
			PORTD &= ~(1<<(vl6_queue[i]->port_D_pin));
		}
	}
	
	checkSPI();
	
	_delay_ms(2);
	
	TWI_Master_Initialise();

	for(int i = 0; i < vl6_count; i++) {
		// Pull corresponding GPIO0 high
		if(vl6_queue[i]->port_B_pin >= 0) {
			PORTB |= (1<<(vl6_queue[i]->port_B_pin));
		} else if(vl6_queue[i]->port_C_pin >= 0) {
			PORTC |= (1<<(vl6_queue[i]->port_C_pin));
		} else if(vl6_queue[i]->port_D_pin >= 0) {
			PORTD |= (1<<(vl6_queue[i]->port_D_pin));
		}
		
		_delay_ms(1);
		
		// Configure the sensor
		vl6180x_init(vl6_queue[i], DEF_SLAVE_ADDR + 2*(i+1));
	
	}
	
	// Timer overflows every 10ms
	TCCR1B |= (1<<WGM12);
	OCR1A = 1250;
		
	TCCR1B |= (1<<CS11) | (1<<CS10);
		
	static uint8_t slow_inits = 0;
	for(int i = 0; i < vl6_count; i++) {
		// Set up interleave mode on the sensor
		start_interleave(vl6_queue[i]);
		
		// Want to space out readings to finish at 10ms intervals, rather than all at same time
		if((TIFR1 & (1 <<OCF1A ))) {
			slow_inits++;
		} else {
			// Wait for timer overflow
			while(!(TIFR1 & (1 <<OCF1A )));
		}
		
		// Reset timer
		TIFR1 = (1<<OCF1A);
	}
	
	// Let first device finish measurement
	for(int i = 0; i < (10-vl6_count); i++) {
		while(!(TIFR1 & (1 <<OCF1A ))) {
			checkSPI();	
		}
				
		// Reset timer
		TIFR1 = (1<<OCF1A);
	}
/*		
	vl6180x lcd;
	lcd.addr = 0x50;
	write_byte(&lcd, 0x0C48, 0x45);	
	*/
	static uint32_t slow_reads = 0;
	while(1) {		
		for(int i = 0; i < vl6_count; i++) {
			
			for(int j = 0; j < 2; j++) {
				checkSPI();
				// Read from the sensor
				result = get_data(vl6_queue[i],j);
			}
			
			// Flash LED every 1 second
			global_count++;
			if(global_count >= 100) {
				PORTB ^= (1<<PB1);
				global_count = 0;
/*				write_byte(&lcd, 0x0C0C,0x0C);
				uint16_t MSBs = 0x3030 | ((left_back_flat.distance / 100) << 8) | ((left_back_flat.distance / 10) % 10);
				uint8_t LSB = 0x30 | (left_back_flat.distance % 10); 
				write_byte(&lcd, MSBs, LSB); */
			}

			if(TIFR1 & (1 <<OCF1A )) {// Read took too long
				slow_reads = (slow_reads + 1) % UINT32_MAX;		
			} else {// Wait for timer to expire 
				while(!(TIFR1 & (1 <<OCF1A ))) {
					checkSPI();
				}
			}
			
			// Reset timer
			TIFR1 = (1<<OCF1A);				
		}
		// Let first device finish new measurement		
		for(int i = 0; i < (10 - vl6_count); i++) {
			while(!(TIFR1 & (1 <<OCF1A ))){
				checkSPI();
			}
			global_count++;
			// Reset timer
			TIFR1 = (1<<OCF1A);
		} 
//		wdt_reset();
	} 
	return 0;
}

// Indicate gripper wants data
ISR(INT0_vect) {
	spiFlag = 1;
}