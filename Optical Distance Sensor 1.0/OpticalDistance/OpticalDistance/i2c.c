#include <util/delay.h>
#include <avr/io.h>
#include "TWI_MASTER.h"
#include "i2c.h"

#ifndef TWI_READ
#define TWI_READ 0x1
#endif

#ifndef TWI_WRITE 
#define TWI_WRITE 0x0
#endif

extern uint8_t TWI_transBuff[10], TWI_recBuff[10];

// data: Holds the slave address and address of register to read from
// receive: Holds the slave address before, will hold slave address and data after
// n: The number of bytes to read
void read_bytes(uint8_t * data, uint8_t * receive, uint8_t n)
{
	TWI_Start_Transceiver_With_Data(data,3);
	
	while(TWI_Transceiver_Busy());

	TWI_Start_Transceiver_With_Data(receive,n+1);
	
	while(TWI_Transceiver_Busy());
	
	TWI_Get_Data_From_Transceiver(receive);
}

// The number of bytes to write (not including address)
void write_bytes(uint8_t * data, uint8_t n)
{
	TWI_Start_Transceiver_With_Data(data,n + 1);
}

void write_byte(uint8_t slave_addr, uint16_t reg_addr, uint8_t data)
{
	// First byte is slave address indicating a write
	TWI_transBuff[0] = slave_addr & ~(TWI_READ);
	
	// Load register to write to
	TWI_transBuff[1] = reg_addr >> 8;
	TWI_transBuff[2] = reg_addr & 0x00FF;
	
	// Load the data to be written
	TWI_transBuff[3] = data;
	
	TWI_Start_Transceiver_With_Data(TWI_transBuff,4);
}