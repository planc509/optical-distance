#!/usr/bin/env python
# Derived from pub_optical.py (By Liang-Ting Jiang)
# Publishes data recieved by gripper

import rospy
from std_msgs.msg import ByteMultiArray
from optical_dist.msg import OpticalDist

data = OpticalDist()
ts = [0,0,0,0]
seq = 0
count = 0;

rospy.init_node("optical_distance_publisher", anonymous=True)
side = rospy.get_param('~side', 'right')
n_sense = int(rospy.get_param('~n_sense',"2"))
rospy.loginfo("Started publisher for the %s gripper", side)
if side == 'right':
	sub_topic = 'raw_pressure/r_gripper_motor'
elif side == 'left':
	sub_topic = 'raw_pressure/l_gripper_motor'
else:
	print side + ' is not a valid side parameter'
	raise

pub_topic = 'optical/' + side
pub = rospy.Publisher(pub_topic, OpticalDist)
stor = [0] * (3*n_sense)
def rawDataCallback(msg):
	global count, stor,data,seq,ts
	if msg.data[:4] != ts and msg.data[4] != 0 and msg.data[4] == msg.data[5]:
		ts = msg.data[:4]
		stor[3*(msg.data[4] - 1) : 3*(msg.data[4])] = [(d + 256 if d < 0 else d) for d in msg.data[6:9]]
		print stor
 
		sense_data = [(d + 256 if d < 0 else d) for d in msg.data[0:3]]#[4:4+3]]
#		stor[count:count+3] = sense_data[0:]
		seq += 1
		data.header.seq = seq
		data.header.stamp = rospy.Time.now()
		data.dist = [0] * (2 * n_sense)

		# First reading is in last spot. Then sequential
		for i in range(-2,3*(n_sense - 1),3):
#			blah = sense_data[i+1] << 8 | sense_data[i+2]

			data.dist[(2*i+4)/3] = stor[i+1] << 8 | stor[i+2]
			data.dist[(2*i+7)/3] = stor[i]

#			data.dist[i+1] = sense_data[i]
#			data.dist[i+2] = sense_data[i+1] << 8 | sense_data[i+2]
#		if seq % 1 == 0 and msg.data[4] != 0 and msg.data[4] == msg.data[5]:
	#		print [(d + 256 if d < 0 else d) for d in msg.data[4:(4+5)]]
		pub.publish(data)
		count = (count + 3) % (3*n_sense)

sub = rospy.Subscriber(sub_topic, ByteMultiArray, rawDataCallback)
rospy.spin()