#include <avr/io.h>
#include <avr/interrupt.h>
#include "vl6180x.h"

extern const uint8_t vl6_count;
extern vl6180x * vl6_queue[10];
extern uint8_t spiFlag;

// Setup SPI
void spi_setup(void) {
	
	/**** EXTERNAL INTERRUPT *****/
	DDRD &= ~(1<<DDRD2); //setup External interrupt 0 as Input (PD2) (clear bit 2)
	PORTD |= (1<<PD2); //set PD2(INT0) to high (impedance) (pull-up resistor is activated)
	EIMSK |= (1<<INT0);  //enable INT0
	EICRA |= (1<<ISC01); //INT0 triggered on falling edge

	/********** SPI ***********/
	DDRB |= (1<<DDRB4); // MISO output
	SPCR = (1<<SPE); // Enable SPI

}

// Responds to the masters request for data. Transmits back 5 values.
void processSPI(void) {
	
	// Will get dropped
	int temp = SPDR;
	SPDR = temp;
	while(!(SPSR & (1<<SPIF)));
	temp = SPDR;
	
	// Get requested sensor index and send it back
	SPDR = temp;
	while(!(SPSR & (1<<SPIF)));
	temp = SPDR;
	
	// Get requested sensor index and send it back
	SPDR = temp;
	while(!(SPSR & (1<<SPIF)));
	temp = SPDR;
	
	// Send data from indicated sensor
	for(int i = 0; i < 3; i++)
	{
		
		if(i == 0) {
			SPDR = vl6_queue[(temp - 1) % vl6_count]->distance;
		} else if(i == 1){
			SPDR = (vl6_queue[(temp - 1) % vl6_count]->als) >> 8;
		} else if(i == 2) {
			SPDR = (vl6_queue[(temp - 1) % vl6_count]->als) & 0x00FF;
			break;
		}
		while(!(SPSR & (1<<SPIF)));
		temp = SPDR;
		
	}

}
/*
int count = 0;
void processSPI(void) {
	
	int temp = SPDR;
	SPDR = temp;
	while(!(SPSR & (1<<SPIF)));	
	temp = SPDR;
	
	SPDR = temp;
	while(!(SPSR & (1<<SPIF)));
	temp = SPDR;
	
	SPDR = temp;
	while(!(SPSR & (1<<SPIF)));
	temp = SPDR;
	
	// Send ready to read to
	for(int i = 0; i < 2; i++)
	{
		count++;
		SPDR = (i+2)* temp;
		while(!(SPSR & (1<<SPIF)));
		temp = SPDR;		
	}
	count++;
	SPDR = 4* temp;

}*/

// Quick method to check if the gripper wants data
void checkSPI(void) {
	if(spiFlag) {
		processSPI();
		spiFlag = 0;
	}
}