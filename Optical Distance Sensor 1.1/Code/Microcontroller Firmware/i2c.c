#include <util/delay.h>
#include <avr/io.h>
#include <compat/twi.h>
#include "vl6180x.h"
#include "i2c.h"
#include "spi.h"

extern uint8_t spiFlag;

// Derived from code provided by "NYC Resistor - Using the TWI/I2C Interface"

// Microcontroller clock frequency
#ifndef CPU_FREQ
#define CPU_FREQ 8000000
#endif

#ifndef TWI_READ
#define TWI_READ 0x1
#endif

#ifndef TWI_WRITE
#define TWI_WRITE 0x0
#endif

// Freq of SCL line
#ifndef TWI_FREQ
#define TWI_FREQ 100000
#endif

// Initialize I2C
void TWI_Master_Initialise(void)
{
	checkSPI();
	TWBR = ((CPU_FREQ / TWI_FREQ) - 16) / 2;
	TWCR = _BV(TWEN);
}

// Helper method for I2C
uint8_t TWI_Poll(uint8_t cmd)
{

	
	TWCR = cmd | _BV(TWINT) | _BV(TWEN);
	if(cmd == _BV(TWSTO)) return 0;	
	while(!(TWCR & _BV(TWINT)));
	return (TWSR & 0xf8);
}

// Write data to the reg_addr of the sensor corresponding to vl6Ptr
// vl6Ptr must have slave address in addr field
uint8_t write_byte(vl6180x * vl6Ptr, uint16_t reg_addr, uint8_t data)
{
	uint8_t cmd;
	uint8_t status = 0;
	
	checkSPI();
	
	cmd = TWI_Poll(_BV(TWSTA)); // Send start
	if(cmd != TW_START && cmd != TW_REP_START) return 0;
	TWDR = vl6Ptr->addr | TWI_WRITE; // Send Write Address
	if(TWI_Poll(0) != TW_MT_SLA_ACK) goto release;
	TWDR = reg_addr >> 8; // Send MSB of Reg Address
	if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	TWDR = reg_addr & 0x00FF; // Send LSB of Reg Address
	if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	TWDR = data; // Send data
	if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	status = 1;
	release:
	TWI_Poll(_BV(TWSTO));
	return status;
}

// vl6Ptr: Must have transmit buffer loaded with the following
//		slave address at index 0 
//		register address to write to at indices 1 and 2
//		data at subsequent indices
// n: The number of bytes to write to vl6Ptr (including slave and reg address)
uint8_t write_bytes(vl6180x * vl6Ptr, uint8_t n)
{
	if(n < 4)
	{
		return 0;
	}
	uint8_t cmd;
	uint8_t status = 0;
	
	checkSPI();
	
	cmd = TWI_Poll(_BV(TWSTA)); // Send start
	if(cmd != TW_START && cmd != TW_REP_START) return 0;
	TWDR = vl6Ptr->transmit[0] | TWI_WRITE; // Send Write Address
	if(TWI_Poll(0) != TW_MT_SLA_ACK) goto release;
	TWDR = vl6Ptr->transmit[1]; // Send MSB of Register Address
	if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	TWDR = vl6Ptr->transmit[2]; // Send LSB of Register Address
	if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	for(int i = 3; i < n; i++) // Send data
	{
		TWDR = vl6Ptr->transmit[i]; // Send LSB of Register Address
		if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	}
	status = 1;
	release:
	TWI_Poll(_BV(TWSTO));
	return status;
}
// vl6Ptr: Must have transmit buffer loaded with the following
//		slave address at index 0
//		register address to read from at indices 1 and 2
// It must also have the receive buffer loaded with the (read) slave address at the first index
// Will have data in subsequent bit after read
// n: The number of bytes to read from vl6Ptr
uint8_t read_bytes(vl6180x * vl6Ptr, uint8_t n)
{
	checkSPI();
	
	uint8_t cmd;
	uint8_t status = 0;
	uint8_t poll_result;
	cmd = TWI_Poll(_BV(TWSTA)); // Send start
	if(cmd != TW_START && cmd != TW_REP_START) return 0;
	TWDR = vl6Ptr->transmit[0] | TWI_WRITE; // Send slave address
	poll_result = TWI_Poll(0);
	if(poll_result != TW_MT_SLA_ACK) goto release;
	TWDR = vl6Ptr->transmit[1]; // Send MSB of reg address
	if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	TWDR = vl6Ptr->transmit[2];  // Send LSB of reg address
	if(TWI_Poll(0) != TW_MT_DATA_ACK) goto release;
	if(TWI_Poll(_BV(TWSTA)) != TW_REP_START) goto release;  // Send restart
	TWDR = vl6Ptr->receive[0] | TWI_READ;  // Send slave address
	if(TWI_Poll(0) != TW_MR_SLA_ACK) goto release;
	int i;
	for(i = 1; i < n; i++) {
		if(TWI_Poll(_BV(TWEA)) != TW_MR_DATA_ACK) goto release;
		vl6Ptr->receive[i] = TWDR; // Read bit
		
	}
	if(TWI_Poll(0) != TW_MR_DATA_NACK) goto release;
	vl6Ptr->receive[i] = TWDR; // Read last bit
	status = 1;
	
	release:
	TWI_Poll(_BV(TWSTO));
	return status;
}